/**
 * @module routes/account
 * @requires controllers/pets
 * @description Bootstraps account-related API endpoints
 * @example [
 * routes.post('/activate'),
 * routes.post('/create'),
 * routes.post('/update'),
 * routes.post('/remove'),
 * routes.get('/fetch'),
 * ]
 * */
