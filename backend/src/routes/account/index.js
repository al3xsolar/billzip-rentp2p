/**
 * @module routes/account
 * @requires controllers/account
 * @description Bootstraps account-related API endpoints
 * */

/**
 * @module routes/account
 * @requires controllers/account
 * @description Bootstraps account-related API endpoints
 */

/* Third Party Dependencies */
const routes = require('express').Router()

/* Custom Dependencies */
const accountControllers = require('../../controllers/account')
const middlewares = require('../../middlewares')

// routes.post('/confirm', accountControllers.confirm)
// routes.post('/login', accountControllers.login)
routes.post('/register', accountControllers.register)
// routes.post('/forgot', accountControllers.forgot)
// routes.post('/checkCode', accountControllers.checkCode)
// routes.post('/resetPassword', accountControllers.resetPassword)
// routes.post('/sessionCheck', middlewares.authenticate, accountControllers.sessionCheck)
// routes.post('/update', middlewares.authenticate, accountControllers.update)

module.exports = routes

