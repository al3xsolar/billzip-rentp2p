/**
 * @module routes/account
 * @requires controllers/account
 * @description Bootstraps account-related API endpoints
 * @example [
 * routes.post('/confirm'),
 * routes.post('/login'),
 * routes.post('/register'),
 * routes.post('/forgot'),
 * routes.post('/checkCode'),
 * routes.post('/resetPassword'),
 * routes.post('/sessionCheck'),
 * routes.post('update'),
 * ]
 * */
