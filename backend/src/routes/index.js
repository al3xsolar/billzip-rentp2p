/**
 * @module routes
 * @requires routes/account
 * @requires util
 * @description Defines API endpoints and creates request => middleware => controller pipeline
 */

/* Third Party Dependencies */
const routes = require('express').Router()

/* Custom Dependencies */
const accountRoutes = require('./account')

const middlewares = require('../middlewares')
const util = require('../util')

routes.use('/account', accountRoutes)


routes.all('*', function(req, res) {
    return util.general.sendResponse(res, "error", {
        message: "Unauthorized Request"
    })
})

module.exports = routes
