/**
 * @module middlewares/multipart
 * @description Middleware for handling multipart image uploads.
 * @requires config
 * @requires util
 * @returns {Function}
 */
