/**
 * @module middlewares/use-third-party
 * @description Attach third-party middleware to Express app instance.
 * @param app Express app object
 * @returns {Function}
 */

/* Custom Dependencies */
const config = require('../config')
const util = require('../util')

/* Third Party Dependencies */
const bodyParser = require('body-parser')
const helmet = require('helmet')
const hpp = require('hpp')
const secure = require('express-force-https')

module.exports = function(app) {
    if (!app) {
        throw new Error("Express app object is required")
    }
    if (config.environment.ssl && config.environment.ssl === true) { // force SSL connections on production
        app.use(secure)
    }
    app.use(helmet()) // Provides additional security. See: https://www.npmjs.com/package/helmet
    app.use(bodyParser.json()) // Turn incoming req.body requests into JSON
    app.use(hpp()) // Prevent HTTP Parameter-Pollution Attacks

    /* Catch Errors */
    app.use((err, req, res, next) => {
        if (err instanceof SyntaxError &&
            err.status >= 400 && err.status < 500 &&
            err.message.indexOf('JSON')) {
            return util.general.sendResponse(res, "error", {
                message: ["Request must contain valid JSON"]
            })
        }
    })
}
