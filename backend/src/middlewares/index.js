/**
 * @module middlewares
 * @description Returns all middleware files in a single, easy-to-access object
 * @returns {Object}
 */

/* Custom Dependencies */
const authenticate = require('./authenticate')
const cors = require('./cors')
const multipart = require('./multipart')
const permissions = require('./permissions')

const useThirdParty = require('./use-third-party')

module.exports = {
    authenticate,
    cors,
    multipart,
    permissions,
    useThirdParty
}
