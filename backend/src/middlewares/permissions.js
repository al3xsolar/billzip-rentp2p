/**
 * @module middlewares/permissions
 * @description Protects API endpoints based on user role
 * @requires config
 * @requires util
 * @returns {Function}
 */

const config = require('../config')
const util = require('../util')

module.exports = function(permissions) {
    if (!permissions || !util.validate.isArray(permissions)) {
        throw new Error("permissions must be an array")
    } else if (!permissions.length) {
        throw new Error("permissions array must contain values")
    }
    const permissionTypes = config.users.roles.slice(0)
    permissionTypes.push("Public")
    /* Validate submitted permissions */
    for(var i = 0; i < permissions.length; i++) {
        if (!util.validate.isInArray(permissions[i], permissionTypes)) {
            throw new Error("Invalid permission: " + permissions[i])
        }
    }
    return function(req, res, next) {
        if (util.validate.isInArray('Public', permissions)) { //publically accessible endpoint
            return next()
        } else if (!req.USER) {
            throw new Error("req.USER was not found. Protected endpoints require auth() middleware function to be called first.")
        } else if (!req.USER.role || !util.validate.isInArray(req.USER.role, permissions)) {
            return util.general.sendResponse(res, "error", {
                message: "You do not have permission to access this endpoint."
            })
        }
        return next()
    }
}
