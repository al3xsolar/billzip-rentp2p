/**
 * @module middlewares/authenticate
 * @description Requires authentication for API endpoints and sets req.USER
 * @requires util
 * @returns {Function}
 */

/* Custom Dependencies */
const UserService = require('../services').User
const util = require('../util')

/* Variable Declarations */
let decodedToken = {}

module.exports = function(req, res, next) {
    const authenticationHeader = req.headers['authorization']

    if (!authenticationHeader || !util.validate.isString(authenticationHeader)) {
        return util.general.sendResponse(res, "error", {
            message: "Authorization header is required"
        })
    } else if (authenticationHeader.substring(0,7) !== "Bearer ") {
        return util.general.sendResponse(res, "error", {
            message: "Authorization header must follow the 'Bearer <token>' format"
        })
    }
    const token = authenticationHeader.split(" ")[1] || null

    util.general.validateJsonWebToken(token)
        .then((decoded) => {
            decodedToken = decoded
            if (!decodedToken.id) {
                return Promise.reject("Invalid token submitted")
            }
            return UserService.fetch({'id': decodedToken.id})
        })
        .then((userData) => {
            req.USER = userData
            return next()
        })
        .catch((err) => {
            return util.general.sendResponse(res, "error", {
                message: "Invalid token submitted"
            })
        })
}

