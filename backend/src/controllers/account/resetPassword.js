/**
 * @module controllers/account/resetPassword
 * @description API endpoint that resets the password and asks for the reset code generated in forgot
 * @requires services/user
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
