/**
 * @module controllers/account/login
 * @description Provides API endpoint that allows users to login. Returns JSONWebToken.
 * @requires services
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
