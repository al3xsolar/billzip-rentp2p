/**
 * @module controllers/account/confirm
 * @description Provides API endpoint that allows users to confirm their account
 * @requires services/user
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
