/**
 * @module controllers/account
 * @description Provides an object that compiles all the account controllers
 * @returns {Object}
 */

module.exports = {
    register:       require('./register')
}