/**
 * @module controllers/account/sessionCheck
 * @description Provides API endpoint that validates API token
 * @requires services
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
