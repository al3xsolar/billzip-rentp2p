/**
 * @module controllers/account/checkCode
 * @description Provides API endpoint that checks for a user's activation code effectively verifying their email account
 * @requires services/user
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
