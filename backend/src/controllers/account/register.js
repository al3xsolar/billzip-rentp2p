/**
 * @module controllers/account/register
 * @description Provides API endpoint that allows users to create a new account
 * @requires services/user
 * @requires services/email
 * @requires services/payment
 * @requires util
 * @todo determine if we are going to be connecting the payment mechanism pre or post account activation
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */

/* Custom Dependencies */
const config = require('../../config')
const UserService = require('../../services/user')
const util = require('../../util')

module.exports = function(req, res) {
    let createdUser = null
    const userData = {
        email: req.body.email || null,
        password: req.body.password || null,
        first_name: req.body.first_name || null,
        last_name: req.body.last_name || null,
    }
    UserService.create(userData)
        .then((newUser) => {
            if (newUser) {
                createdUser = newUser
            }
            return null;
        })
        .then(() => {
            return console.log("Created new user " + JSON.stringify(createdUser))
        })
        .then(() => {
            /* Send Email for Confirmation */
            return util.general.sendResponse(res, "success", {
                message: "Account created! A confirmation email was sent to " + req.body.email + "."
            })
        })
        .catch((err) => {
            console.log(err);
            if (createdUser) {
                return UserService.remove({
                    'id': createdUser.id
                })
                    .then(()=>{
                        return util.general.sendResponse(res, "fail",{
                            message: [err]
                        })
                    })
            } else {
                return util.general.sendResponse(res, "fail", {
                    message: [err]
                })
            }
        })
}