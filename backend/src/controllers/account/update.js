/**
 * @module controllers/account/update
 * @description Provides API endpoint that update users
 * @requires services/user
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
