/**
 * @module controllers/account/checkCode
 * @description Provides API endpoint that fetches the user by email and resets
 * their password by matching a randomly generated string that we email to them
 * with the generated String in the frontend
 * @requires services/user
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
