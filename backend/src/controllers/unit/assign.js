/**
 * @module controllers/unit/assign
 * @description API endpoint that assigns a unit to a user. This also helps not repeat the creation of units so that they can be passed around multiple times
 * @requires services/user
 * @requires services/unit
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
