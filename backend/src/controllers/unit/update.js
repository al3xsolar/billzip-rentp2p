/**
 * @module controllers/unit/update
 * @description API endpoint that allows users to update properties
 * @param req Express request object
 * @param res Express response object
 * @requires services/property
 * @requires services/email
 * @requires util
 * @returns {Function}
 */
