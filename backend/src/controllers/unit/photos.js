/**
 * @module controllers/unit/photos
 * @description API endpoints for adding and removing unit photos
 * @requires services/units
 * @requires services/upload
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
