/**
 * @module controllers/unit/fetch
 * @description API endpoint that fetches a
 * @requires services/property
 * @requires services/unit
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
