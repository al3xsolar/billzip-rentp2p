/**
 * @module controllers/unit/create
 * @description API endpoint that creates a unit
 * @requires services/property
 * @requires services/unit
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
