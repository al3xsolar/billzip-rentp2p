/**
 * @module controllers/properties/delete
 * @description API endpoint that deletes a unit
 * @requires services/unit
 * @requires services/email
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
