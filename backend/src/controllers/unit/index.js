/**
 * @module controllers/pets
 * @description Provides an object that compiles all the unit controllers
 * @returns {Object}
 */
