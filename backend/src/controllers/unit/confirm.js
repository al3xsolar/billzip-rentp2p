/**
 * @module controllers/unit/confirm
 * @description API endpoint that confirms that a unit has been created under a property
 * @requires services/property
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
