/**
 * @module controllers/properties
 * @description Provides an object that compiles all the property controllers
 * @returns {Object}
 */
