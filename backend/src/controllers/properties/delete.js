/**
 * @module controllers/properties/delete
 * @description API endpoint that deletes a property
 * @requires services/property
 * @requires services/email
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */

