/**
 * @module controllers/properties/fetch
 * @description API endpoint that fetches all properties depending on the filter passed into the request.
 * @requires services/property
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
