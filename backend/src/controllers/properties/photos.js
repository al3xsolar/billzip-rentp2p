/**
 * @module controllers/properties/photos
 * @description API endpoints for adding and removing property photos
 * @requires services/property
 * @requires services/upload
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */

