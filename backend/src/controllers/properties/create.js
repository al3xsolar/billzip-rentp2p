/**
 * @module controllers/properties/create
 * @description API endpoint that creates a property
 * @requires services/property
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
