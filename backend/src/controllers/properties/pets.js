/**
 * @module controllers/properties/pets
 * @description API endpoint that activates the pets model under the property
 * @requires services/property
 * @requires services/pets
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
