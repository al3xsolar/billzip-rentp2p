/**
 * @module controllers/properties/confirm
 * @description API endpoint that confirms the creation of a new property via Email
 * @requires services/property
 * @requires services/email
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
