/**
 * @module controllers/pets/activate
 * @description API endpoint that activates a property for pets or deactivates it making the pets model null
 * @requires services/pet
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
