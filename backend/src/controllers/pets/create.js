/**
 * @module controllers/pets/create
 * @description API endpoint that creates a pet and assigns it an owner unit
 * @requires services/pet
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
