/**
 * @module controllers/pets/update
 * @description API endpoint that updates the pet model without deleting previous pets
 * @requires services/pet
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
