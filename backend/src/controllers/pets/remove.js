/**
 * @module controllers/pets/remove
 * @description API endpoint that removes a pet from a unit but maintains the record of the pet
 * @requires services/pet
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
