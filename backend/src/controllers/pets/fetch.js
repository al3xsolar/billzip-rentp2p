/**
 * @module controllers/pets/fetch
 * @description API endpoint that fetches all pets by property and assigns their owner's unit.
 * @example 'pets': [{
 *    'pet1': {
 *        ownerUnitId: A
 *    }
 * }]
 * @requires services/pet
 * @requires util
 * @param req Express.js request object
 * @param res Express.js response object
 * @returns {Function}
 */
