/**
 * @module controllers/pets
 * @description Provides an object that compiles all the pet controllers
 * @returns {Object}
 */
