var fs        = require('fs');
var path      = require('path');
var Sequelize      = require('sequelize');
var basename  = path.basename(__filename);
var db        = {};

const config = require('../../config')
const conn = require('../conn')

var sequelize = conn

fs
    .readdirSync(__dirname)
    .filter(file => {
      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes)
      db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.conn = sequelize;
db.op = sequelize.Op;

/**
 * Monkey patch issue causing deprecation warning when customizing allowNull validation error
 *
 * See https://github.com/sequelize/sequelize/issues/1500
 */
Sequelize.Validator.notNull = function (item) {
  return !this.isNull(item);
}

module.exports = db;
