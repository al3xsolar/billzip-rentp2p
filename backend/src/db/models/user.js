/**
 * @module db/models/user
 * @description Instantiates Sequelize user model
 * @param sequelize
 * @param DataTypes
 * @returns {Function}
 */

const config = require('../../config');
const util = require('../../util');

module.exports = (sequelize, DataTypes) => {
    let User = sequelize.define('user', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        email: {
            type: DataTypes.STRING,
            unique: { // changing requires migration
                args: true,
                fields: ['email'],
                msg: "Email Address is already in use"
            },
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Email is required'
                },
                isEmail: {
                    msg: 'Invalid Email Address format'
                }
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Password is required'
                },
                len: {
                    args: [32, 128],
                    msg: "Invalid password"
                }
            }
        },
        activation_code: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: util.general.generateString(128)
        },
        last_login: {
            type: DataTypes.DATE,
            allowNull: true
        },
        first_name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'First Name is required'
                },
                len: {
                    args: [1, 30],
                    msg: "Invalid First Name length"
                }
            }
        },
        last_name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'Last Name is required'
                },
                len: {
                    args: [1, 30],
                    msg: "Invalid Last Name length"
                }
            }
        },
        // birthday: {
        //     type: DataTypes.STRING,
        //     allowNull: true,
        //     validate: {
        //         isBirthday: function (val, next) {
        //             if (!util.validate.isBirthdayString(val)) {
        //                 throw new Error("Invalid Birthday syntax")
        //             }
        //             return next()
        //         }
        //     }
        // },
        phone_number: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: {
                    args: [7, 18],
                    msg: "Invalid Phone Number length"
                }
            }
        },
        company_name: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: {
                    args: [1, 30],
                    msg: "Invalid Company Name length"
                }
            }
        },
        company_address_street: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: {
                    args: [1, 50],
                    msg: "Invalid Street Address length"
                }
            }
        },
        company_address_city: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: {
                    args: [1, 50],
                    msg: "Invalid City length"
                }
            }
        },
        company_address_state: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: {
                    args: [1, 50],
                    msg: "Invalid State length"
                }
            }
        },
        company_address_zip: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: {
                    args: [5, 7],
                    msg: "Invalid Zip Code format"
                }
            }
        }
    }, { timestamps: false });

    User.associate = function (models) {
        /* Create associations here */
    };

    return User
};
