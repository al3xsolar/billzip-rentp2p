/**
 * @module db/config/sequelize-config
 * @desc Contains Sequelize configuration object. (Used by CLI via .sequelizerc)
 * @return {Object}
 */

/* Custom Dependencies */
const config = require('../../config')

module.exports = {
    dialect: 'mysql',
    timezone: '-05:00', // for writing to database
    database: config.db.name,
    username: config.db.username,
    password: config.db.password,
    host: config.db.host,
    port: config.db.port,
    logging: false,
}
