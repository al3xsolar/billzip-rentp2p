/**
 * @module db
 * @description Returns database-related modules
 * @requires conn
 * @requires models
 * @returns {Object} {init, models}
 */

/* Import Custom Dependencies */
const conn = require('./conn')
const models = require('./models')

module.exports = {
    conn,
    models
}
