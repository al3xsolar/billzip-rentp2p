/**
 * @module db/conn
 * @description Contains sequelize db connection
 * @requires db/config/sequelize-config
 * @returns {Promise}
 */

const Sequelize = require('sequelize');
const sequelizeConfig = require('./config/sequelize-config')

const conn = new Sequelize(sequelizeConfig);

module.exports = conn
