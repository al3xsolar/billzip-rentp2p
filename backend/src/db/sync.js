/**
 * @module db/sync
 * @description Syncs DB tables to model format
 * @requires db/conn
 * @requires db/models
 * @returns {Promise}
 */

const conn = require('./conn')
const models = require('./models') // required because attachment wont happen otherwise

conn.sync().then((msg) => {
    console.log("Database has been synced to model format")
    return process.exit()
})
