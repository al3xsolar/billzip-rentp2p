/**
 * @module config/production
 * @description Overrides production config values for development environment
 * @returns {Object}
 */

const config = {
    appName: "RentBeets",
    appDeveloper: "Material Exchange Inc",
    appDomain: "rentbeets.com",
    db: {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        name: process.env.DB_NAME,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD
    },
    environment: {
        name: process.env.NODE_ENV,
        port: process.env.PORT || 3000
    },
    jwt: {
        secret: process.env.JWT_SECRET
    },
}

module.exports = config