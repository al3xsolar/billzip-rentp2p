/**
 * @module config
 * @description Returns environment-based configuration object.
 * @returns {Object}
 */

/* Third Party Dependencies */
require('dotenv').config()

const env = process.env.NODE_ENV
const environments = ['local', 'development', 'production', 'test', 'qa']

if (environments.includes(env)) {
    configFile = require('./' + env)
} else {
    configFile = require('./local')
}

module.exports = configFile
