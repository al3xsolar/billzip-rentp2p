/**
 * @module config/test
 * @description Overrides production config values for test environment
 * @returns {Object}
 */
const config = require('./production')

/* Override config here */
module.exports = config
