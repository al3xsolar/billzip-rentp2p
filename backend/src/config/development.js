/**
 * @module config/development
 * @description Overrides production config values for development environment
 * @returns {Object}
 */

const config = require('./production')

/* Override config here */


module.exports = config