/**
 * @module config/local
 * @description Overrides production config values for local environment
 * @returns {Object}
 */

const config = require('./production')

/* Override config here */
config.appDomain = "http://localhost:8000"

module.exports = config