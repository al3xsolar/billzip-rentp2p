/**
 * @module util
 * @description Returns util files for easy access
 * @returns {Object}
 */

/* Import Custom Dependencies */
const general = require('./general')
const validate = require('./validate')

module.exports = {
    general,
    validate
}
