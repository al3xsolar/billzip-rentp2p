/**
 * @module util/validate
 * @description General input validation functions
 * @returns {Object}
 */

const isArray = function(input) {
    return Array.isArray(input);
}

const isInArray = function(needle, haystack) {
    if (needle === undefined) {
        throw new Error("needle must be defined")
    } else if (!isArray(haystack)) {
        throw new Error("haystack must be an array")
    } else if (haystack.length < 1) {
        return false
    }
    if (haystack.indexOf(needle) !== -1) {
        return true
    } else {
        return false
    }
}

const isBoolean = function(input) {
    return typeof input === 'boolean'
}

/**
 * @function
 * @param {*} input Any input type
 * @description Determines whether or not a given input is a valid birthday. 'mm/dd/yyyy'. Algorithm courtesy of https://stackoverflow.com/a/6178341/6867304
 * @returns {Boolean}
 */
const isBirthdayString = function(dateString) {
    var currentTime = new Date()
    var currentYear = parseInt(currentTime.getFullYear())
    // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false
    // Parse the date parts to integers
    var parts = dateString.split("/")
    var day = parseInt(parts[1], 10)
    var month = parseInt(parts[0], 10)
    var year = parseInt(parts[2], 10)
    // Check the ranges of month and year
    if(year < (currentYear - 100) || year > currentYear || month == 0 || month > 12)
        return false
    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ]
    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29
    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1]
}

const isFunction = function(input) {
    return typeof input === 'function'
}

const isEmail = function(input) {
    if (!input) {
        return false
    }
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(input)
}

const isNumber = function(input) {
    return typeof input === 'number'
}

const isObject = function(input) {
    return typeof input === 'object'
}

const isString = function(input) {
    return typeof input === 'string'
}

const isStringWithinLength = function(string, minimum, maximum) {
    if (!string || !isString(string)) {
        throw new Error("string must be a string")
    } else if (!isNumber(minimum) || minimum < 0) {
        throw new Error("minimum must be a positive number")
    } else if (!isNumber(maximum) || maximum < 0) {
        throw new Error("maximum must be a positive number")
    } else if (minimum > maximum) {
        throw new Error("maximum must be greater than minimum")
    }
    if (string.length >= minimum && string.length <= maximum) {
        return true
    }
    return false
}

module.exports = {
    isArray,
    isInArray,
    isBirthdayString,
    isBoolean,
    isEmail,
    isFunction,
    isNumber,
    isObject,
    isString,
    isStringWithinLength
}
