/**
 * @module util/general
 * @description This file contains general utility functions
 * @returns {Object}
 */

/* Custom Dependencies */
const config = require('../config')

/* Third Party Dependencies */
const asyncLoop = require('node-async-loop')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

/**
 * @function
 * @description Returns current unix time since epoch in seconds format
 * @returns {String}
 */
const currentUnixTime = function() {
    return Math.round(Date.now() / 1000).toString()
}

/**
 * @function
 * @description Returns a signed JSON web token that contains passed object
 * @returns {Promise} resolve(token)
 */
const createJsonWebToken = function(object) {
    return new Promise((resolve, reject) => {
        jwt.sign(object, config.jwt.secret, function(err, token) {
            if (err) {
                return reject("Could not sign token")
            }
            return resolve(token)
        })
    })
}

/**
 * @function
 * @description Compares a string/salt hash to another provided hash
 * @returns {Promise} resolve() or reject(err)
 */
const hashCompare = function(password, hash) {
    return new Promise(function(resolve, reject) {
        let result = bcrypt.compareSync(password, hash)
        resolve(result)
    })
}

/**
 * @function
 * @description Returns the hash of a string
 * @param {String} string String that will be hashed
 * @returns {String}
 */
const hashString = function(str) {
    if (!str || typeof str !== 'string') {
        throw new Error("Invalid str")
    }
    let salt = bcrypt.genSaltSync(10)
    let hash = bcrypt.hashSync(str, salt)
    return hash
}

/**
 * @function
 * @description Returns a random string of specified length
 * @param {Number} length Length of new string
 * @returns {String}
 */
const generateString = function(length) {
    if (!length || typeof length !== 'number') {
        length = 64
    }
    var key = ""
    var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    for (var i = 0; i < length; i++) {
        key += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return key
}

/**
 * @function
 * @description Returns a random string of numbers
 * @param {Number} length Length of new string
 * @returns {String}
 */
const generateNumberString = function(length) {
    if (!length || typeof length !== 'number') {
        length = 64
    }
    var key = ""
    var possible = "0123456789"
    for (var i = 0; i < length; i++) {
        key += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return key
}

/**
 * @function
 * @description Returns request IP address
 * @param {Object} req Express.js request object
 * @returns {String}
 */
const getIpAddress = function(req) {
    if (!req) {
        throw new Error("req object is required")
    }
    return req.headers['x-forwarded-for'] || req.connection.remoteAddress
}

/**
 * @function
 * @description Loops asynchronously though an array.
 * @param {Array} array Array of items to loop
 * @param {Function} callback Function to run on each item. function(item, next)
 * @returns {Promise}
 */
const loopAsync = function(array, callback) {
    if (!array || !typeof array === 'array') {
        throw new Error("array must be an array")
    } else if (!callback || typeof callback !== 'function') {
        throw new Error("callback must be a function")
    }
    return new Promise((resolve, reject) => {
        asyncLoop(array, callback, function() {
            return resolve()
        })
    })
}

/**
 * @function
 * @description Parses sequelize errors and returns clean messages
 * @param {Array} err Array of errors
 * @returns {Array}
 */
const parseSequelizeErrors = function(err) {
    if (!err || typeof err !== 'object') {
        return ["Something went wrong"]
    }
    let cleanErrors = []
    for (i = 0; i < err.length; i++) {
        if (err[i].message) {
            cleanErrors.push(err[i].message)
        }
    }
    return cleanErrors
}

/**
 * @function
 * @description Standardizes server response format
 */
const sendResponse = function(res, status, data) {
    const statuses = ['success', 'fail', 'error']
    if (!res || typeof res !== "object") {
        throw new Error("res must be an object")
    } else if (!res.send || typeof res.send !== "function") {
        throw new Error("res.send must be a function")
    } else if (status !== "success" && status !== "fail" && status !== "error") {
        throw new Error("Invalid status specified")
    } else if (!data || typeof data !== "object") {
        throw new Error("data must be an object")
    }
    return res.send({
        status: status,
        data: data
    })
}

/**
 * Validates JSON web token against key
 * @function
 * @returns {Promise} resolve(decoded or null), rejecrt(err)
 */
const validateJsonWebToken = function(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.jwt.secret, function(err, decoded) {
            if (err) {
                return reject("Could not validate token")
            }
            return resolve(decoded)
        })
    })
}

const addListingEmailFields = function (l) {
    l.sheet = l.type == 'sheet';
    l.roll = l.type == 'roll';
    l.nice_type = l.type.charAt(0).toUpperCase() + l.type.slice(1);
    l.length_unit = l.type=='sheet' ? 'in' : 'ft';

    return l;
};

module.exports = {
    createJsonWebToken,
    currentUnixTime,
    hashCompare,
    hashString,
    generateString,
    generateNumberString,
    getIpAddress,
    loopAsync,
    parseSequelizeErrors,
    sendResponse,
    validateJsonWebToken,
    addListingEmailFields,
}
