/**
 * @module controllers
 * @description Returns all of our controllers in a single, easy-to-access object
 * @returns {Object}
 */

/* Third Party Dependencies */

const express = require('express')
const app = express()

/* Custom Dependencies */

const config = require('./config')
const routes = require('./routes')
const db = require('./db')
const middlewares = require('./middlewares')

/* Use Third Party Middlewares */
middlewares.useThirdParty(app)

/* Use Custom Middlewares */
app.use(middlewares.cors)

/* Connect And Sync DB */
db.conn.sync()
    .then(function() {
        console.log("Database Connection Established!")

        /* When testing is needed activate the test file here */

        /*
        * if (config.environment.name == 'local') {
        *   const testFile = require('./testFile')
        * return
        * */

    })
    .then(function() {
        app.listen(config.environment.port, function(err) {
            if (err) {
                console.log( config.appName + " could not be started on localhost:" + config.environment.port )
                process.exit()
            }
            console.log( config.appName + " is running on localhost:" + config.environment.port )
        });
    })
    .catch((err) => {
        throw new Error(err)
    })

/* Usage */
app.use('/api', routes);
app.get('/*', function(req, res) {
    res.send(
        "Hello World!"
    )
})

module.exports = app;

