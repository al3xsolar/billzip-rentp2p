/**
 * @module services/user
 * @description Simple user service that abstracts user CRUD operations
 * @requires db/models
 * @requires util
 * @return {Class}
 * */

/* Custom Dependencies */
const models = require('../db/models')
const UserModel = models.user
const util = require('../util')

class User {

    /**
     * Authenticate a user and return a JSON web token
     * @function
     * @param {String} emailAddress User Email Address
     * @param {String} password User plaintext password
     * @returns {Promise} resolve({msg, token}), reject(err)
     * */

    static authenticate(emailAddress, password) {
        let userData = null
        return new Promise((resolve, reject) => {
            if (!emailAddress || !util.validate.isEmail(emailAddress)) {
                return reject("email address is required")
            } else if (!password || !util.validate.isString(password)) {
                return reject("Password is required")
            } else if (!util.validate.isStringWithinLength(password, 5, 20)) {
                return reject("Invalid password length")
            }
            UserModel.findOne({
                'where': {
                    'email': emailAddress
                }
            })
                .then((result) => {
                    if (!result) {
                        return reject("Invalid Email Address or Password")
                    }
                    userData = result
                    return util.general.hashCompare(password, result.password)
                })
                .then((isPasswordValid) => {
                    if (!isPasswordValid) {
                        return reject("Invalid Email Address or Password")
                    }
                    return util.general.createJsonWebToken({
                        id: userData.id,
                        email: userData.email,
                        role: userData.role
                    })
                })
                .then((token) => {
                    return resolve({
                        message: "Logged In!",
                        token: token,
                        id: userData.id,
                        role: userData.role,
                        first_name: userData.first_name,
                        last_name: userData.last_name,
                        email: userData.email
                    })
                })
                .catch((err) => {
                    console.dir(err)
                    return reject("Invalid Email Address or Password")
                })
        })
    }

    /**
     * Create new user
     * @function
     * @returns {Promise}
     */

    static create(userData) {
        if (!userData || !util.validate.isObject(userData)) {
            throw new Error("userData must be an object")
        }
        if (userData["password"]) {
            userData["password"] = util.general.hashString(userData["password"])
        }
        return new Promise((resolve, reject) => {
            UserModel.create(userData)
                .then((createdUser) => {
                    return resolve(createdUser)
                })
                .catch((err) => {
                    return reject(util.general.parseSequelizeErrors(err.errors) || "Could not create user")
                })
        })
    }

    /**
     * Confirm user
     * @function
     * @returns {Promise}
     */

    static confirm(confirmData) {
        if (!confirmData || !util.validate.isObject(confirmData)) {
            throw new Error("confirmData must be an object")
        }
        let userData = null
        return new Promise((resolve, reject) => {
            UserModel.findOne({where: confirmData})
                .then(result => {
                    if (!result) {
                        return reject("Invalid email or code")
                    }
                    userData = result
                    return UserModel.update({activation_code: null}, {where: confirmData})
                })
                .then((updatedUser) => {
                    return util.general.createJsonWebToken({
                        id: userData.id,
                        email: userData.email
                    })
                })
                .then((token) => {
                    return resolve({token: token, role: userData.role, id: userData.id})
                })
                .catch((err) => {
                    return reject("Could not confirm user")
                })
        })
    }

    /**
     * Fetch user
     * @function
     * @param {Object} [queryData] Sequelize query object
     * @param {Boolean} [single] Fetch one or many?
     * @returns {Promise}
     */

    static fetch(queryData = {}, single = true) {
        if (queryData && !util.validate.isObject(queryData)) {
            throw new Error("queryData must be an object")
        } else if (single && !util.validate.isBoolean(single)) {
            throw new Error("single must be a boolean")
        }
        return new Promise((resolve, reject) => {
            let queryMethod = null
            let query = {where: queryData}

            if (single) {
                queryMethod = UserModel.findOne(query)
            } else {
                queryMethod = UserModel.findAll(query)
            }

            queryMethod
                .then(result => {
                    if (!result) {
                        return resolve(null)
                    }
                    return resolve(result)
                })
                .catch((err) => {
                    return reject("Could not fetch user")
                })
        })
    }

    /**
     * Remove user
     * @function
     * @param {Object} [queryData] Sequelize query object
     * @returns {Promise}
     */

    static remove(queryData = {}) {
        if (queryData && !util.validate.isObject(queryData)) {
            throw new Error("queryData must be an object")
        }
        return new Promise((resolve, reject) => {
            let query = {where: queryData}
            UserModel.destroy(query)
                .then(result => {
                    if (!result) {
                        return resolve(null)
                    }
                    return resolve(result)
                })
                .catch((err) => {
                    return reject("Could not delete user")
                })
        })
    }

}

module.exports = User
