/**
 * @module services
 * @requires services/email
 * @requires services/user
 * @description Bundles all services together for easy access
 * @returns {Object} {Email, User, ...}
 */

module.exports = {
    User: require('./user'),
/*    Email: require('./email')*/
}