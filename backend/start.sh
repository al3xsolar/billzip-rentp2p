# Project Startup Script. Called with NPM Start.
if [ "$NODE_ENV" == "production" ] || [ "$NODE_ENV" == "development" ] || [ "$NODE_ENV" == "qa" ] ; then
  node src/index.js
else
  nodemon src/index.js
fi